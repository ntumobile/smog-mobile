#-*- coding: utf-8 -*-
# multipart form
# http://code.activestate.com/recipes/146306-http-client-to-post-using-multipartform-data/

# NOTE: Within app engine, httplib is implemented through GAE urlfetch library.
#

import httplib, mimetypes

def post_multipart(host, selector, fields, files, cookie):
    """
    Post fields and files to an http host as multipart/form-data.
    fields is a sequence of (name, value) elements for regular form fields.
    files is a sequence of (name, filename, value) elements for data to be uploaded as files
    Return the server's response page.
    """
    content_type, body = encode_multipart_formdata(fields, files)
    h = httplib.HTTP(host)
    h.putrequest('POST', selector)
    h.putheader('content-type', content_type)
    h.putheader('content-length', str(len(body)))
    h.putheader('Cookie', cookie)
    h.endheaders()
    h.send( body.decode('string_escape') )
    errcode, errmsg, headers = h.getreply()
    return h.file.read()

def encode_multipart_formdata(fields, files):
    """
    fields is a sequence of (name, value) elements for regular form fields.
    files is a sequence of (name, filename, value) elements for data to be uploaded as files
    Return (content_type, body) ready for httplib.HTTP instance
    """
    BOUNDARY = '----------ThIs_Is_tHe_bouNdaRY_$'
    CRLF = '\r\n'
    L = []
    for (key, value) in fields:
        L.append('--' + BOUNDARY)
        L.append('Content-Disposition: form-data; name="%s"' % key)
        L.append('')
        L.append(value.encode('string_escape') ) # encode to make CRLF.join() happy
    for (key, filename, value) in files:
        L.append('--' + BOUNDARY)
        L.append('Content-Disposition: form-data; name="%s"; filename="%s"' % (key, filename))
        if filename != '':
            L.append('Content-Type: %s' % get_content_type(filename))
        L.append('')
        L.append(value.encode('string_escape') ) # encode to make CRLF.join() happy
    L.append('--' + BOUNDARY + '--')
    L.append('')

    body = CRLF.join(L)
    # CRLF.join would emit UnicodeDecodeError upon raw binary codes or Chinese characters.
    # UnicodeDecodeError would also occur on simple string concatenation using operator '+'.
    # Therefore we use encode & decode to bypass this error, though encode & decode might be
    # expensive in terms of computation.

    content_type = 'multipart/form-data; boundary=%s' % BOUNDARY
    return content_type, body

def get_content_type(filename):
    return mimetypes.guess_type(filename)[0] or 'application/octet-stream'

