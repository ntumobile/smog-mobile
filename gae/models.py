import logging
from google.appengine.ext import db

class SmogCase(db.Model):
    '''
    SmogCase -- an entry of a case of polluting car
    Including submitter's information and case information.
    Column names are aligned with the government website.
    '''
    # submitter's information
    App_Name    = db.StringProperty()
    App_Add     = db.StringProperty() # address
    App_Unit    = db.StringProperty() # the city or county
    App_Coun    = db.StringProperty() # the county or region
    App_id      = db.StringProperty() # social id
    App_Tel     = db.StringProperty()
    App_Tel2    = db.StringProperty() # night phone
    App_Tel3    = db.StringProperty() # cell phone
    App_FAX     = db.StringProperty() # Fax number
    App_Email   = db.StringProperty() # Email
    IsReply     = db.StringProperty() # method to reply
    IsGetMoney  = db.IntegerProperty() # whether to get money
    PayType     = db.IntegerProperty() # 1 for hand pickup, 2 for wiretransfer
    v_headbanks = db.StringProperty() # bankname
    BankAccount = db.StringProperty() # bankaccount

    #case information
    IdenNo      = db.StringProperty() # the identity number on the website
    Car_No1     = db.StringProperty() # license plate number (the part before '-')
    Car_No2     = db.StringProperty() # license plate number (the part after '-')
    Car_Kind      = db.StringProperty() # check docs for the types
    App_CarColor  = db.StringProperty()
    App_CarColor2 = db.StringProperty()
    App_CarColor3 = db.StringProperty()
    Find_Add   = db.StringProperty()
    Find_Unit   = db.StringProperty() # attributes used in GET request string.
    Find_Coun   = db.StringProperty()

    # information used within this system
    # using traditional nameing convention for columns here.
    location    = db.GeoPtProperty()
    time        = db.DateTimeProperty(required=True)
    status      = db.StringProperty()
    cookie      = db.StringProperty() # cookie string
    case_number = db.StringProperty() # case identity number returned by government
                                      # after all submission is successful.
          

class ImageEntry(db.Model):
    case        = db.ReferenceProperty(SmogCase)
    filename    = db.StringProperty()
    picture     = db.BlobProperty(default=None)
    time        = db.DateTimeProperty(required=True)
    #add uploader?

class VideoEntry(db.Model):
    case        = db.ReferenceProperty(SmogCase)
    filename    = db.StringProperty()
    video       = db.BlobProperty(default=None)
    time        = db.DateTimeProperty(required=True)
    #add uploader?

class Log(db.Model):
    case        = db.ReferenceProperty(SmogCase)
    content     = db.TextProperty()
    time        = db.DateTimeProperty(required=True)
