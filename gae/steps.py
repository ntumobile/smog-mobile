#-*- coding: utf-8 -*-
# The three steps to case submission

import re, urllib, datetime
from google.appengine.api import urlfetch
from multiformpost import post_multipart
from models import SmogCase, ImageEntry, Log

def values_from(case, attributes):
    value = {}
    for attr in attributes:
        v = case.__getattribute__(attr)
        if v != '':
            value[attr] = unicode(v).encode('big5')

    return value

def steps(key):
    '''
        Complete step2 and step3, given the key of the case that passed step1.
    '''
    case = SmogCase.get(key)

    step2( case=case )
    return step3( case=case, debug=False )

def step1(case):
    '''
        Given case id (key to SmogCase instance in datastore);
        Submits user data, retrieves cookie and IdenNo, and update SmogCase
        instance
    '''
    url = "http://polcar.epa.gov.tw/public/connepb_input_check.asp"
    values = values_from(case, [ \
        'App_Name', 'App_Unit', 'App_Coun', 'App_Add', 'App_Tel', 'IsReply', 'IsGetMoney', \
        'App_id', 'App_Tel2', 'App_Tel3', 'App_FAX', 'App_Email', 'PayType', \
        'v_headbanks', 'BankAccount'])

    data = urllib.urlencode(values)
    response = urlfetch.fetch(url=url, payload=data, method=urlfetch.POST,
        follow_redirects=False)

    try:
        iden_no = re.search(r'\?IdenNo=(.*)' ,response.headers['Location']).group(1)
    except KeyError: # server did not return a redirect pate
        # 2 POSSIBILITIES:

        # 1. some columns are not filled in correctly
        err = re.search(r"alert\('([^']*)'\)",response.content)
        if err is not None:
            raise Exception(err.group(1).decode('big5'))

        # 2. the government doubts some columns are suspicous, and hands in a confirm
        #    page.
        else:
            err = re.search(r"confirm\('([^']*)'\)", response.content).group(1)
            raise Exception(err.decode('big5') )


    cookie = response.headers['set-cookie'].split(';')[0] + ";visitNews=1;visitMsg=1"

    case.IdenNo = iden_no
    case.cookie = cookie
    case.status = u"個人資訊已上傳至政府"
    # update IdenNo in database
    case.put()


def step2(case):
    '''
        submission part two (picture upload)
    '''

    def strip_tokens(body):
        return (
            re.search('"__VIEWSTATE" value="([^"]*)"', body).group(1),
            re.search('"__EVENTVALIDATION" value="([^"]*)"',body).group(1)
        )

    #
    # fetch new cookie: ASP.NET_Sessionid (an HTTP-Only cookie)
    # Response: terms of copyright, we users should accept it.
    #
    url = "http://polcar.epa.gov.tw/PolcarWeb/Public/Up_Picture.aspx?IdenNo=%s" % case.IdenNo
    resp = urlfetch.fetch(url, headers={'Cookie': case.cookie})
    if resp.headers.has_key('set-cookie'):
        case.cookie = case.cookie + ";%s;" % resp.headers['set-cookie'].split(';')[0]
    viewstate,eventvalidation = strip_tokens(resp.content)

    #
    # fetch hidden veiwstats (CSRF tokens)
    # Response: upload page (the page with five upload slots)
    #
    fields = urllib.urlencode({
      'hideIsConform':'', 'btnConformYes': '同意',
      '__VIEWSTATE':viewstate, '__EVENTVALIDATION': eventvalidation
    })
    resp = urlfetch.fetch(url, headers={'Cookie': case.cookie},
                          payload=fields, method=urlfetch.POST)
    viewstate,eventvalidation = strip_tokens(resp.content)

    #print "viewstate: ", viewstate
    #print "eventvalidation: ", eventvalidation

    #
    # upload files
    #

    images = ImageEntry.all().filter('case =', case).fetch(5)

    files = []
    for i in range(0, len(images) ):
      files.append( ('FileUpload'+str(i+1), images[i].filename, images[i].picture) )
    #print files

    fields = [
      ('__VIEWSTATE',  viewstate),
      ('__EVENTVALIDATION', eventvalidation),
      ('But_UpPhoto', '上傳照片' )
    ]

    url_pieces = url.split('/', 3)
    # something like ['http:', '', 'polcar.epa.gov.tw', 'PolcarWeb/Public/Up_Picture.aspx?IdenNo=<SESS_ID>']

    resp = post_multipart(
      host=url_pieces[2],
      selector='/' + url_pieces[3],
      fields=fields,
      files=files,
      cookie=case.cookie
    )

    #print resp
    resp = re.findall('<span id="Lab_Message\d"><font color="Red">([^<]*)<' , resp)
    case.status = u"上傳至政府：" + (u"，").join( [r.decode('utf8') for r in resp] )
    case.put()
    return resp

def step3(case, debug=False):
    '''
    Submit the complete case to local EPA
    '''

    def timestamp_ROC():
        '''
          Gives current time stamp, using 民國紀年 (YYYMMDDHHMMSS)
        '''
        return str(datetime.date.today().year-1911) + \
               datetime.datetime.now().strftime("%m%d%H%M%S")

    #
    # first choose the local EPA that we should report to.
    #

    url_unit = "http://polcar.epa.gov.tw/public/connepb_input_findunit.asp?Find_Unit=%s&IdenNo=%s" % \
               (case.Find_Unit, case.IdenNo)
    #print url_unit
    resp = urlfetch.fetch(url_unit, headers={'Cookie':case.cookie}, follow_redirects=False)
    #print resp.headers

    #
    # then submit to this local EPA
    #
    #url_submit = "http://polcar.epa.gov.tw/public/connepb_input_review.asp"
    url_submit = "http://polcar.epa.gov.tw/public/pub1_4.asp?IdenNo=%s" % case.IdenNo
    values = values_from(case, ['IdenNo', 'Car_No1', 'Car_No2', 'Car_Kind', 'Find_Coun', 'Find_Add',
      'App_CarColor', 'App_CarColor2', 'App_CarColor3'])

    values['Start_App_Date'] = timestamp_ROC() #hidden
    values['Find_Date'] = str(case.time.year - 1911) + case.time.strftime("%m%d")
    values['Find_Hour'] = str(case.time.hour)
    values['Find_Minute'] = str(case.time.minute)
    values['Pol_Fact'] = 'E'
#    values['Pol_Memo'] = (u'這是 NTU HCI Lab 的專題計畫的測試紀錄。計畫完成後，我們將釋出免費 app 讓民眾能輕鬆地用智慧型手機快速通報烏賊車案件。造成不便還請多多包含！').encode('big5')
    values['Pol_Memo'] = (u'烏賊終結者 APP 舉報車輛').encode('big5')
    values['submit'] = (u'確定檢舉').encode('big5')

    url_pieces = url_submit.split('/', 3)
    # something like ['http:', '', 'polcar.epa.gov.tw', 'PolcarWeb/Public/Up_Picture.aspx?IdenNo=<SESS_ID>']

    if debug: # if debug, do not send out posts to the government.
        case.status = u'[DEBUG]已完成報案'
        case.put()
        return values

    resp = post_multipart(
      host=url_pieces[2],
      selector='/' + url_pieces[3],
      fields=values.items(),
      files=[],
      cookie=case.cookie
    )

    #print resp

    #
    # Lastly, mimic the javascript form submit done within pub1_5.asp
    #

    url_confirm = "http://polcar.epa.gov.tw/public/pub1_5.asp"

    values = re.findall(r"Name='([^']*)' Value='([^']*)'", resp)
    data = urllib.urlencode(dict(values) )
    resp = urlfetch.fetch(url_confirm, headers={'Cookie': case.cookie}, payload=data,
           method=urlfetch.POST)

    # get case identity number on the government side
    try:
        case.case_number = re.findall('<font color="#FF0000">([^<]*)</font>', resp.content)[0]
        case.status = u"報案完成"
    except IndexError:
        log = Log(time=datetime.datetime.now())
        log.case = case
        log.content = resp.content.decode('big5')
        log.put()
        case.case_number = '<ERROR>'
        case.status = u'無法取得案件編號，報案失敗'
    case.put()

    return case

