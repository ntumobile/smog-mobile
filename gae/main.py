#!/usr/bin/env python
#-*- coding: utf-8 -*-

from google.appengine.ext import webapp, db, deferred
from google.appengine.ext.webapp import template, util
from google.appengine.runtime.apiproxy_errors import RequestTooLargeError
from django.utils import simplejson

from steps import step1, steps
from models import SmogCase, ImageEntry, VideoEntry

import re, operator, datetime

def current_time():
    return datetime.datetime.utcnow() + datetime.timedelta(hours=8)

def assign_from_request(case, request, attributes):
    '''set case datastore object from reqeust object'''
    for attr in attributes:
        case.__setattr__(attr, request.get(attr))

def print_error(self, errstr=""):
    self.response.out.write('{"error":"%s"}' % ( errstr) )

class MainHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write( template.render('index.html', []) )

class CaseHandler(webapp.RequestHandler):
    '''Reports a case of a squid car'''

    def get(self):
        try:
            case = SmogCase.get( self.request.get('key') )
        except db.BadKeyError:
            print_error(self, u"key 不正確")
            return
        images = ImageEntry.all().filter('case = ', case).fetch(5)
        videos = VideoEntry.all().filter('case = ', case).fetch(5)
        self.response.out.write( simplejson.dumps( {
          'status': case.status,
          'case_number': case.case_number,
          'images': [i.key().id() for i in images],
          'videos': [v.key().id() for v in videos]
        } ) )
    def post(self):
        try:
            key = self.request.get('key')
            case = None
            if key and key is not None:
                try:
                    case = SmogCase.get(key)
                except db.BadKeyError:
                    raise Exception(u'key 不正確')
            else: # not in database yet
               # should also check if the case is already going through the submission process
                try:
                    timestr = self.request.get('datetime')
                    time_format = "%Y-%m-%d:%H:%M"
                    time = datetime.datetime.strptime(timestr, time_format) #may need to catch ValueError if the format is wrong
                except ValueError: # catch case when required fields are not filled in.
                    raise Exception(u'時間 "%s" 格式錯誤' % timestr)

                # create the case
                case = SmogCase(time=time)

            # assign a bunch of attributes from the request object
            assign_from_request(case, self.request, [ \
              'App_Name', 'App_Tel', 'App_Add', 'App_Unit', 'App_Coun', 'IsReply',
              'App_id', 'App_Tel2', 'App_Tel3', 'App_FAX', 'App_Email', 'v_headbanks',
              'BankAccount', 'Car_No1', 'Car_No2', 'Car_Kind', 'App_CarColor', 
              'App_CarColor2', 'App_CarColor3', 'Find_Add', 'Find_Unit', 'Find_Coun'
            ])

            try:
                case.location = db.GeoPt(float(self.request.get('lat')),
                                         float(self.request.get('lng')))
            except ValueError:
                pass 
                # no need to handle if lat & lng is not set,
                # since the geolocation is not required.

            case.IsGetMoney = 0 # required
            try:
                case.IsGetMoney = int(self.request.get('IsGetMoney'))
                case.PayType = int(self.request.get('PayType'))
            except ValueError:
                raise Exception(u'付款欄位錯誤')

            validcar_types = "A,B,C,D,E,F,Z1".split(',')
            if case.Car_Kind not in validcar_types+[""]:
                raise Exception(u'車種錯誤')

            #http://maps.googleapis.com/maps/api/geocode/json?latlng=25.0193211,121.5414986&sensor=true

            # final check on necessary columns
            if 0 < reduce(operator.mul, map(len, [ \
                  case.App_Name, case.App_Tel, case.App_Unit, case.IsReply ]) ): 
                # none of them are empty , fill in the rest
                # check conditional fields
                # check media -- if all okay, submit to taskqueue
                case.status = (u"基本資料已上傳，送出至政府中") # the data is ready
                case.put()    # save status immediately.
            else:
                raise Exception(u"有必填欄位為空")

            # submit user info to the government, and get IdenNo of the case
            step1(case)  # get IdenNo and save
            ret = {
                'key': str(case.key()),
                'status': case.status
            }
            self.response.out.write(simplejson.dumps(ret))

        except Exception, e: # python 2.5 syntax
            e = ': '.join(e.args)
            if case is not None:
                case.status = e 
                case.put()
            print_error(self, e)


class NewCaseHandler(webapp.RequestHandler):
    def get(self):
        """new submit new case for testing"""
        self.response.out.write(template.render('submitcase.html',[]))

class CompleteHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write('<form method="POST" action=".">key: <input type="text" name="key" /><input type="submit" /></form>')
    def post(self):
        # call steps to the government in the background
        deferred.defer(steps, self.request.get('key'))
        self.response.out.write(simplejson.dumps({"status":u"媒體檔案傳送至政府中"}))

class ImageHandler(webapp.RequestHandler):
    def get(self):
        try:
            pictureid = int(self.request.get('id'))
        except ValueError:
            pictureid = None
        if not pictureid:
            retstr = "20 random images: <br>\n"
            images = ImageEntry.all().fetch(20)
            #should set keys_only to true
            for i in images:
                retstr += "<a href=\"./?pictureid=%s\">image %s</a>\n<br>"%(i.key().id(),i.key().id())
            self.response.out.write(retstr)
            return
        img = ImageEntry.get_by_id(pictureid)
        if (img and img.picture):
            self.response.headers['Content-Type'] = 'image/jpeg' #should save type as well
            self.response.out.write(img.picture)
        else:
            self.error(404)
    def post(self):
        try:
            case = SmogCase.get(self.request.get("key"))
        except:
            print_error(self, u"key 不正確")
            return

        img = ImageEntry(time=current_time())
        try:
            img.picture = self.request.get("image") # may be too large
        except MemoryError:
            print_error(self, u"上傳的檔案超過記憶體限制")

        img.filename = self.request.POST["image"].filename
        img.case = case
        try:
            img.put()
            images = ImageEntry.all().filter('case = ', case).fetch(5)
            images = [i.key().id() for i in images]
            self.response.out.write(simplejson.dumps({'images': images}))
        except RequestTooLargeError:
            print_error(self, u"上傳的檔案超過資料庫限制")


class VideoHandler(webapp.RequestHandler):
    def get(self):
        pass    
    def post(self):
        try:
            case = SmogCase.get( self.request.get("key") )
        except:
            self.response.out.write('key 不正確')
            return

        vid = VideoEntry(time=current_time())
        try:
            vid.video = self.request.get("video") # may be too large
        except MemoryError:
            print_error(self, u"上傳的檔案超過記憶體限制")

        vid.case = case
        try:
            vid.put()
            videos = VideoEntry.all().filter('case =', case)
            videos = [i.key().id() for i in videos]
            self.response.out.write(simplejson.dumps({'videos': videos}))
        except RequestTooLargeError:
            print_error(self, u"上傳的檔案超過資料庫限制")


class NewImageHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write(template.render('image.html', []))

class NewVideoHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write("""
        <html>
            <body> Upload your video
              <form action="." enctype="multipart/form-data" method="post">
                <div><label>key: <input name="key"/></label></div>
                <div><input type="file" name="video"/></div>
                <div><input type="submit" value="upload video"></div>
              </form>
            </body>
          </html>""")

def main():
    application = webapp.WSGIApplication([
        (r'/', MainHandler),
        (r'/case/', CaseHandler),
        (r'/case/new/', NewCaseHandler),
        (r'/case/complete/', CompleteHandler),
        (r'/image/', ImageHandler),
        (r'/image/new/', NewImageHandler),
        (r'/video/', VideoHandler),
        (r'/video/new/', NewVideoHandler),
    ],debug=True)

    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()

